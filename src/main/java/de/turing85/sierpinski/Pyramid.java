package de.turing85.sierpinski;

import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import de.turing85.javafx.scene.Scene3D;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import de.turing85.javafx.shape.Tetrahedron;
import javafx.stage.Stage;

/**
 * A little JavaFX 3D sample application, showing the Sierpinski pyramid.
 *
 * @author Marco Bungart
 */
public class Pyramid extends Application {

    private static final PhongMaterial red = new PhongMaterial(Color.RED);

    private static final double INITIAL_HEIGHT = 1024;

    static {
        red.setSpecularColor(Color.VIOLET);
    }

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        final Group root = new Group();
        final Scene3D scene = new Scene3D(root, 1024, 768, true);

        scene.setFill(Color.AQUA);
        primaryStage.setScene(scene);
        primaryStage.show();

        scene.startCameraDrive(200, 2500);

        new Thread(() -> {
            Tetrahedron tetra = new Tetrahedron(INITIAL_HEIGHT);
            Group sierpinski = new Group();

            sierpinski.setTranslateY(-INITIAL_HEIGHT / 2.0);

            Platform.runLater(() -> root.getChildren().add(sierpinski));

            sierpinski(INITIAL_HEIGHT,
                    tetra.getUpperCorner(),
                    tetra.getLeftCorner(),
                    tetra.getRightCorner(),
                    tetra.getBackCorner(),
                    sierpinski);
        }).start();

        new Thread(() -> {
            try {
                Thread.sleep(TimeUnit.MILLISECONDS.convert(18, TimeUnit.SECONDS));
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }

            scene.stopCameraDrive();
        }).start();
    }

    /**
     * This method does the actual computation of the pyramid. The height is divided in every step.
     * The termination condition is {@code height <= 16}. Please remember that  every tetrahedron will
     * produce four new tetrahedrons until the termination condition is reached. For
     * {@link #INITIAL_HEIGHT} {@code = 1024} this means that seven divisions will be done, and
     * therefore {@code 4^7 = 16_384}
     *
     * @param height (initial) height of the outermost pyramid
     * @param up     upper corner of the pyramid.
     * @param left   (lower) left corner of the pyramid.
     * @param right  (lower) right corner of the pyramid.
     * @param back   (lower) back corner of the pyramid.
     * @param world  a {@link Group} to hang in all created {@link Tetrahedron}s.
     */
    private void sierpinski(final double height,
                            final Point3D up,
                            final Point3D left,
                            final Point3D right,
                            final Point3D back,
                            final Group world) {

        if (height <= 16) {
            final Tetrahedron tetra = new Tetrahedron(height);
            tetra.setTranslateX(up.getX());
            tetra.setTranslateY(up.getY());
            tetra.setTranslateZ(up.getZ());

            tetra.getMeshView().setMaterial(red);

            Platform.runLater(() -> world.getChildren().add(tetra));

        } else {
            final Point3D middleUpLeft = up.add(left).multiply(0.5);
            final Point3D middleUpRight = up.add(right).multiply(0.5);
            final Point3D middleUpBack = up.add(back).multiply(0.5);
            final Point3D middleLeftRight = left.add(right).multiply(0.5);
            final Point3D middleLeftBack = left.add(back).multiply(0.5);
            final Point3D middleRightBack = right.add(back).multiply(0.5);
            final double newHeight = height / 2.0;

            sierpinski(newHeight, up, middleUpLeft, middleUpRight, middleUpBack, world);
            sierpinski(newHeight, middleUpLeft, left, middleLeftRight, middleLeftBack, world);
            sierpinski(newHeight, middleUpRight, middleLeftRight, right, middleRightBack, world);
            sierpinski(newHeight, middleUpBack, middleLeftBack, middleRightBack, back, world);
        }
    }
}