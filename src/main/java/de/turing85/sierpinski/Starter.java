package de.turing85.sierpinski;

public class Starter {
    public static void main(String... args) {
        System.setProperty("prism.forceGPU", "true");
        Pyramid.main(args);
    }
}
