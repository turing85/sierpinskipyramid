package de.turing85.javafx.shape;

import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

/**
 * A representation of a regular
 * <a href="https://en.wikipedia.org/wiki/Tetrahedron" target="_blank">Tetrahedron</a>.
 * <p>
 * Internally, the Tetrahedron is stored as {@link MeshView}.
 *
 * @author Marco Bungart
 */
public class Tetrahedron extends Group {

    public static final double SQRT_3_HALVES = Math.sqrt(1.5);

    /**
     * The upper corner of the Tetrahedron.
     */
    private final Point3D upperCorner;

    /**
     * The (lower) left corner of the Tetrahedron.
     */
    private final Point3D leftCorner;

    /**
     * The (lower) right corner of the Tetrahedron.
     */
    private final Point3D rightCorner;

    /**
     * The (lower) back corner of the Tetrahedron.
     */
    private final Point3D backCorner;

    /**
     * The MeshView takes care of the visual representation.
     */
    private final MeshView meshView;

    /**
     * Construct a Tetrahedron with the given height. The height is the lowest distance from the
     * triangle forming the bottom to the upper corner. The upper corner will be at (0, 0, 0) with
     * the base facing in positive-Y direction (should be downwards in most cases).
     *
     * @param H the height of the Tetrahedron.
     */
    public Tetrahedron(final double H) {
        this.upperCorner = new Point3D(0, 0, 0);
        this.leftCorner = new Point3D(-SQRT_3_HALVES * H / 2.0, H, -SQRT_3_HALVES * H / 3.0);
        this.rightCorner = new Point3D(SQRT_3_HALVES * H / 2.0, H, -SQRT_3_HALVES * H / 3.0);
        this.backCorner = new Point3D(0, H, SQRT_3_HALVES * 2.0 * H / 3.0);
        TriangleMesh mesh = new TriangleMesh();
        this.meshView = new MeshView(mesh);

        mesh.getPoints().addAll((float) this.upperCorner.getX(),
                (float) this.upperCorner.getY(),
                (float) this.upperCorner.getZ(),

                (float) this.leftCorner.getX(),
                (float) this.leftCorner.getY(),
                (float) this.leftCorner.getZ(),

                (float) this.rightCorner.getX(),
                (float) this.rightCorner.getY(),
                (float) this.rightCorner.getZ(),

                (float) this.backCorner.getX(),
                (float) this.backCorner.getY(),
                (float) this.backCorner.getZ());

        mesh.getTexCoords().addAll(0, 0,
                0, 1,
                1, 0,
                1, 1);

        mesh.getFaces().addAll(0, 0, 2, 2, 1, 1,
                0, 0, 3, 3, 2, 2,
                0, 0, 1, 1, 3, 3,
                1, 1, 2, 2, 3, 3);

        this.meshView.setDrawMode(DrawMode.FILL);
        this.getChildren().addAll(this.meshView);
    }

    /**
     * Getter for {@link #meshView}.
     *
     * @return the {@link MeshView} representing the triangles of the Tetrahedron.
     */
    public MeshView getMeshView() {
        return (this.meshView);
    }

    /**
     * Getter for {@link #upperCorner}
     *
     * @return a copy of the upper corner of the Tetrahedron.
     */
    public Point3D getUpperCorner() {
        return (this.upperCorner.multiply(1));
    }

    /**
     * Getter for {@link #leftCorner}
     *
     * @return a copy of the (lower) left corner of the Tetrahedron.
     */
    public Point3D getLeftCorner() {
        return (this.leftCorner.multiply(1));
    }

    /**
     * Getter for {@link #rightCorner}
     *
     * @return a copy of the (lower) right corner of the Tetrahedron.
     */
    public Point3D getRightCorner() {
        return (this.rightCorner.multiply(1));
    }

    /**
     * Getter for {@link #backCorner}
     *
     * @return a copy of the (lower) back corner of the Tetrahedron.
     */
    public Point3D getBackCorner() {
        return (this.backCorner.multiply(1));
    }
}