package de.turing85.javafx.scene;

import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Parent;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;

/**
 * An extension of {@link Scene}. This class initializes some variables (e.g the camera) and
 * provides a basic camera drive around the scene origin, keeping its view on said origin.
 *
 * @author Marco Bungart
 */
public class Scene3D extends Scene {
    /**
     * This point is used internally to do some trigonometric calculations.
     */
    public static final Point3D DEFAULT_ORIENTATION = new Point3D(0.0, 0.0, 1.0);

    /**
     * This Timer is used for the camera drive. While you could start and stop the drive multiple
     * times, this drive was not designed with thin in mind.
     */
    private AnimationTimer cameraDriver;

    /**
     * Constructor.
     *
     * @param root
     *         the {@link Parent}-node for the scene.
     * @param width
     *         initial width of the window.
     * @param height
     *         initial height of the window.
     * @param depthBuffer
     *         set to true if a depth buffer is necessary.
     */
    public Scene3D(Parent root, int width, int height, boolean depthBuffer) {
        super(root, width, height, depthBuffer);

        Camera camera = new PerspectiveCamera(true);
        camera.setNearClip(0.1);
        camera.setFarClip(100_000);
        this.setCamera(camera);
    }

    /**
     * starts the camera drive
     *
     * @param minDistance
     *         while driving around the scene, this is the minimum distance the camera will have
     *         from the origin.
     * @param maxDistance
     *         while driving around the scene, this is the maximum distance the camera will have
     *         from the origin.
     */
    public void startCameraDrive(final double minDistance, final double maxDistance) {
        if (null == this.cameraDriver) {
            final Camera camera = this.getCamera();
            this.cameraDriver = new AnimationTimer() {

                double startTime;

                boolean isFirst = true;

                @Override
                public void handle(long now) {
                    if (this.isFirst) {
                        this.startTime = now;
                        this.isFirst = false;
                    }

                    final double newFastPercent =
                            (((now - this.startTime) / 1_000_000_000.0) / 6.0);
                    final double newSlowPercent = (newFastPercent / 3.0);
                    final double percent = (1.0 + Math.cos(2 * Math.PI * newFastPercent)) / 2.0;
                    final double newDistance = minDistance + (maxDistance - minDistance) * percent;
                    final double newXRadius = Math.cos(2 * Math.PI * newSlowPercent) * newDistance;
                    final double newYRadius = Math.sin(2 * Math.PI * newSlowPercent) * newDistance;
                    final double newXAngleRad = -Math.sin(2 * Math.PI * newFastPercent);
                    final double newYAngleRad = -Math.sin(2 * Math.PI * newFastPercent);
                    final double newZAngleRad = -Math.cos(2 * Math.PI * newFastPercent);
                    final double newX = newXAngleRad * newXRadius;
                    final double newY = newYAngleRad * newYRadius;
                    final double newZ = newZAngleRad * newDistance;
                    final Point3D newLocation = new Point3D(newX, newY, newZ);
                    final Point3D desiredOrientation = newLocation.multiply(-1.0).normalize();
                    final Point3D rotationAxis =
                            DEFAULT_ORIENTATION.crossProduct(desiredOrientation);
                    final double rotationAngleGrad = DEFAULT_ORIENTATION.angle(desiredOrientation);
                    List<Transform> transforms = camera.getTransforms();

                    camera.setTranslateX(0);
                    camera.setTranslateY(0);
                    camera.setTranslateZ(0);
                    transforms.clear();
                    transforms.add(new Translate(newX, newY, newZ));
                    transforms.add(new Rotate(rotationAngleGrad, rotationAxis));
                }

                @Override
                public void stop() {
                    super.stop();

                    this.isFirst = false;
                    this.startTime = 0.0;
                }
            };
        }
        this.cameraDriver.start();
    }

    /**
     * Stop the camera drive.
     */
    public void stopCameraDrive() {
        if (null != this.cameraDriver) {
            this.cameraDriver.stop();
        }
    }
}